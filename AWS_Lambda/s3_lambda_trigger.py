import boto3
import os
import sys
import uuid

s3_client = boto3.client('s3')
s3 = boto3.resource('s3')


def lambda_handler(event, context):
    for record in event['Records']:
        bucket = record['s3']['bucket']['name']
        key = record['s3']['object']['key']

        new_key = key[5:]  # remove "temp/" from front of s3 filename
        # can't actually rename on s3, so instead make a copy
        s3.Object(bucket, new_key).copy_from(CopySource=bucket + '/' + key)
        s3.Object(bucket, key).delete()
        new_obj = s3.Object(bucket, new_key)
        new_obj.Acl().put(ACL='public-read')
