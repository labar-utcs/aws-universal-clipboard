import os
import boto3
import botocore
from boto3 import client
from boto3 import s3
import s3transfer
from static.s3_transfer_class import TransferConfig
from flask import request
from flask import Flask, redirect, url_for, render_template
from flask_bootstrap import Bootstrap
import socket
import time

from geolite2 import geolite2
import geopy.distance

from werkzeug.utils import secure_filename
from werkzeug.contrib.fixers import ProxyFix

application = Flask(__name__)
app = application
s3 = boto3.resource('s3')
s3c = client('s3')
transfer_config = TransferConfig(
    multipart_threshold=5 * 1024 * 1024,
    multipart_chunksize=5 * 1024 * 1024,
    num_download_attempts=10,
    max_request_concurrency=10
)
transfer = s3transfer.S3Transfer(s3c, transfer_config)
file_name_count = 9
last_submitted_item = " "  # TODO: expand this into a list buffer with size 10ish, to cover multiple parallel users

UPLOAD_FOLDER = 'files'
BANNED_EXTENSIONS = set(['php', 'html', 'xhtml', 'exe', 'bat', 'cmd', 'dll', 'jar', 'sys'])
IMAGE_EXTENSIONS = set(['.png', '.jpg', '.jpeg', '.bmp', '.svg', '.gif'])

MAX_GEOLOCATION_MATCH_RANGE = 100  # in miles, based on inaccurate geolocations so needs a lot of buffer distance
CLIPBOARD_FILE_STORAGE_BUCKET_NAME = "myclip.is.contents.clipboards.uniclip"
CLIPBOARD_GEO_LOC_BUCKET_NAME = "myclip.is.geolocations.clipboards.uniclip"
CLIPBOARD_TIMESTAMP_BUCKET_NAME = "myclip.is.timestamps.clipboards.uniclip"
UPLOAD_MESSAGE_FILE_NAME = "UNICLIP_UPLOADING_FILE_MESSAGE_ABC123"
HEALTH_CHECK_PATH = "HEALTH_CODE_CHECK"
TESTING_LOCALLY = False
# TESTING_LOCALLY = True  # TODO: comment out this line when not testing locally
PRODUCTION_BUILD = False

#if not TESTING_LOCALLY:
    #app.wsgi_app = ProxyFix(app.wsgi_app, num_proxies=1)


def create_geolocation_file(gen_code):
    # should check if geolocation_file has already been created and create it if not
    user_ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    reader = geolite2.reader()
    if TESTING_LOCALLY:
        user_ip = '70.112.52.173'
    match = reader.get(user_ip)
    print("match: " + str(match))
    if match is not None:
        user_loc = match['location']
        geolocation_file_text = str(user_loc['latitude']) + '\n' + str(user_loc['longitude']) + '\n' + gen_code
        print(geolocation_file_text)
        s3.Object(CLIPBOARD_GEO_LOC_BUCKET_NAME, gen_code + '.txt').put(Body=geolocation_file_text)


def get_clip_geolocs_s3():
    # should return a list of (lat, long) tuples for clips that are currently still active
    geolocs = []
    bucket = CLIPBOARD_GEO_LOC_BUCKET_NAME
    for obj in s3.Bucket(bucket).objects.all():
        text_string = obj.get()['Body'].read().decode()
        split = text_string.split('\n')
        loc = ([float(split[0]), float(split[1])])
        obj_code = split[2]
        code_is_active = code_currently_active(obj_code)
        if code_is_active:
            # if code is still alive, add it to the list to be tested for distance and added to page if near
            geolocs.append((loc, obj_code))
        else:
            # if code has expired, delete the geolocation entry to prevent it from wasting queries
            s3.Object(CLIPBOARD_GEO_LOC_BUCKET_NAME, obj_code + '.txt').delete()

    return geolocs


def get_nearby_clips(ip_address):
    # should return HTML code for links to clipboards with geolocations near user's IP address
    result_html = ""
    reader = geolite2.reader()
    match = reader.get(ip_address)
    if match is not None:
        user_loc_dict = match['location']
        user_loc = (user_loc_dict['latitude'], user_loc_dict['longitude'])
        locations_to_test = get_clip_geolocs_s3()
        count = 0
        for clip in locations_to_test:
            (clip_loc, clip_code) = clip
            distance = geopy.distance.distance(user_loc, clip_loc).miles
            print(clip_code + ": " + str(distance))
            if distance < MAX_GEOLOCATION_MATCH_RANGE:
                count += 1
                url = "/" + clip_code
                button_html = "<button onclick=\"location.href=\'" + url + "\'\" type=\"button\">" + clip_code + "</button>"
                if count % 5 == 0:
                    button_html = button_html + "<br>\n"
                result_html += button_html
    if len(result_html) > 2:
        # only want to add this if at least 1 nearby clip was found
        result_html = "<br><h3>Nearby Clipboards</h3>\n\n" + result_html
    return result_html


def get_new_clip_code():
    # should return a gen_code for the next empty clipboard designated by the URL-gen microservice

    # create an ipv4 (AF_INET) socket object using the tcp protocol (SOCK_STREAM)
    c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # connect the client
    # client.connect((target, port))
    c.connect(('0.0.0.0', 9999))

    # c.connect((os.environ['clipboard_generation_PORT_9999_TCP_ADDR'], int(os.environ['clipboard_generation_PORT_9999_TCP_PORT'])))

    # send some data
    st = 'GIVE_ME_A_CODE'
    byt = st.encode()
    c.send(byt)

    # receive the response data (4096 is recommended buffer size)
    response = c.recv(1024)

    clip_code = response.decode()
    print(clip_code)

    return clip_code


def submit_item(item, gen_code):
    # should take the item submitted in the form and add it to the s3 bucket associated with the gen_code
    prefix = gen_code + "/contents/"
    global file_name_count
    filename = str(file_name_count) + ".txt"
    global last_submitted_item
    if last_submitted_item == gen_code + str(" TEXT:" + item):
        print("Duplicate text submission")
    else:
        file_name_count += 1
        # self.client.put_object(Bucket=bucketname,Key=directoryname/filename)
        s3.Object(CLIPBOARD_FILE_STORAGE_BUCKET_NAME, prefix + filename).put(Body=item)
        # could maybe include a logistics file in each clipboard bucket/folder
    last_submitted_item = gen_code + str(" TEXT:" + item)


def add_uploaded_file_to_clipboard_s3(gen_code, filename):
    full_path_to_file = UPLOAD_FOLDER + "/" + filename
    prefix = gen_code + "/contents/"
    if filename == "image.jpg":
        global file_name_count
        filename = "image" + str(file_name_count) + ".jpg"
        file_name_count += 1
    upload_message_filename = UPLOAD_MESSAGE_FILE_NAME + "_" + filename + ".txt"
    global last_submitted_item
    if last_submitted_item == gen_code + str(" FILE:" + filename):
        print("Duplicate file submission")
    else:
        # self.client.put_object(Bucket=bucketname,Key=directoryname/filename)
        s3.Object(CLIPBOARD_FILE_STORAGE_BUCKET_NAME, prefix + upload_message_filename).put(
            Body=str(filename) + " is uploading to S3, please wait")

        response = transfer.upload_file(full_path_to_file, CLIPBOARD_FILE_STORAGE_BUCKET_NAME, "temp/" + prefix + filename,
                                        extra_args={'ServerSideEncryption': "AES256", 'ACL': 'public-read'})
        print(response)
        # TODO: find some method to bind purging of UPLOAD_FOLDER files to, otherwise space will run out eventually
    last_submitted_item = gen_code + str(" FILE:" + filename)


def generate_html_from_s3(gen_code):
    # should generate html based on the items in the s3 bucket associated with the gen_code
    result_html = ""
    prefix = gen_code + "/contents/"
    bucket = s3.Bucket(name=CLIPBOARD_FILE_STORAGE_BUCKET_NAME)
    files_not_found = True
    obj_list = bucket.objects.filter(Prefix=prefix)
    get_last_modified = lambda x: int(x.last_modified.strftime('%s'))
    obj_list = sorted(obj_list, key=get_last_modified, reverse=True)
    for obj in obj_list:
        is_text_object = False
        is_image_object = False
        obj_filename = os.path.splitext(obj.key)
        if len(obj_filename) < 2 or len(obj_filename[1]) < 1:
            is_text_object = False
        else:
            if obj_filename[1] == '.txt':
                is_text_object = True
            else:
                is_text_object = False
                if obj_filename[1] in IMAGE_EXTENSIONS and not (UPLOAD_MESSAGE_FILE_NAME in obj_filename):
                    is_image_object = True
        if is_text_object:
            text_string = obj.get()['Body'].read().decode()
            text_lower = str(text_string.lower())

            obj_filename = obj.key
            print(obj_filename)
            skip_obj = False
            if UPLOAD_MESSAGE_FILE_NAME in obj_filename:
                converted_filename = (obj_filename.split(UPLOAD_MESSAGE_FILE_NAME + "_")[1])[:-4]
                print(converted_filename)
                if file_exists_on_s3(CLIPBOARD_FILE_STORAGE_BUCKET_NAME, prefix + converted_filename):
                    skip_obj = True
                    print("Matching file to " + converted_filename + " has been found, deleting and ignoring message...")
                    s3.Object(CLIPBOARD_FILE_STORAGE_BUCKET_NAME, prefix + obj_filename).delete()
            if not skip_obj:
                # test text to see if it is a url and if so transform it into a hyperlink
                if ' ' not in text_lower:
                    if text_lower.startswith('www.'):
                        text_lower = "http://" + text_lower
                    if text_lower.startswith('http://') or text_lower.startswith('https://'):
                        text_string = "<a href=\"" + text_string + "\">" + text_string + "</a>"

                result_html += "<div><div>" + text_string + "</div></div>\n <br> \n"
                files_not_found = False
        else:
            if not is_image_object:
                file_url = '%s/%s/%s' % (s3c.meta.endpoint_url, bucket.name, obj.key)
                file_display_name = str(obj.key).split("/contents/")[1]
                result_html += "<button onclick=\"location.href=\'" + file_url + "\'\" type=\"button\">⇩ &nbsp;" + \
                               file_display_name + "</button><br><br>"
            else:
                # image file
                file_url = '%s/%s/%s' % (s3c.meta.endpoint_url, bucket.name, obj.key)
                result_html += "<input type = \"image\" src = \"" + file_url + "\" onclick=\"location.href=\'" + \
                               file_url + "\'\"  class=\"img_button_class\" /><br><br>"
            files_not_found = False

    if files_not_found:
        print("ALERT", "No file in {0}/{1}".format(bucket, prefix))
    return result_html


@app.route("/")
def hello(clip_items_html="", form_action="", get_near=True, share_button="",
          start_or_add_note="Create a New Clipboard",
          viewing_clipboard_text="", clipboard_code_header="s", return_symbol=""):
    # base page, homepage with interface to add to a new clipboard
    # print("Printing available environment variables")
    # print(os.environ)
    # print("Before displaying index.html")
    file_upload_redirect_url = ""
    if form_action == "":
        new_code = get_new_clip_code()
        form_action = "action=\"" + new_code + "\""
        file_upload_redirect_url = str(request.path) + new_code
    else:
        file_upload_redirect_url = str(request.path)
    nearby_clip_html = ""
    if get_near:
        user_ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
        if TESTING_LOCALLY:
            user_ip = '70.112.52.173'
        nearby_clip_html = get_nearby_clips(user_ip)
    resp = app.make_response(render_template('index.html', clip_items_html=clip_items_html, form_action=form_action,
                           nearby_clip_html=nearby_clip_html, share_button=share_button,
                           start_or_add_note=start_or_add_note, viewing_clipboard_text=viewing_clipboard_text,
                           clipboard_code_header=clipboard_code_header, return_symbol=return_symbol,
                           file_upload_redirect_url=file_upload_redirect_url))
    #resp.set_cookie('last_sub', last_submitted_item)
    return resp


@app.route('/<gen_code>', methods=['GET', 'POST'])
def login(gen_code):
    # should run anytime user adds a new item or accesses a clipboard via link
    if gen_code is None or len(gen_code) < 2:
        return hello()
    if gen_code == HEALTH_CHECK_PATH:
        return render_template('health_check.html'), 201
    if gen_code == 'favicon.ico':
        return render_template('index.html', clip_items_html="", form_action="",
                               nearby_clip_html="", share_button="")
    #global last_submitted_item
    #try:
        #last_submitted_item = request.cookies.get('last_sub')
    #except:
        #print("couldn't find cookie")
        #last_submitted_item = " "
    share_button = ""
    if 'SHARE_LOCAL' in gen_code:
        print("Locally sharing.")
        return share_local_page(gen_code.split("-")[0])
    if request.method == 'POST':
        try:
            message_string = request.form['message']  # this is the string that was input in the message field
            print("Item submitted.")
            submit_item(message_string, gen_code)
        except KeyError as e:
            print("Error: POST but no entry in form")
        try:
            print(request.files)
            file = request.files.keys()
            print(file)
            file = request.files.get(next(file))
            if file.filename == '':
                print('No filename found')
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                print(filename)
                add_uploaded_file_to_clipboard_s3(gen_code, filename)
        except Exception as e:
            print("Error with file upload: " + str(e))
    print(gen_code)

    clip_items_html = generate_html_from_s3(gen_code)
    if not file_exists_on_s3(CLIPBOARD_GEO_LOC_BUCKET_NAME, gen_code + ".txt"):
        share_button = share_local_button_html(gen_code)
    else:
        share_button = share_local_button_html(gen_code, shared_already=True)
    return_symbol = "<a href=\"" + str(url_for("hello")) + \
                    "\" style=\"text-decoration: none; font-size: 1.5em\">&crarr;</a>"
    viewing_clipboard_text = "<br><br><h4>Items stored in Clipboard</h4>"
    viewing_clipboard_text += "<button type=\"button\" class=\"btn btn-default btn-sm\" " \
                              "onClick=\"location=location.href;\"><span class=\"glyphicon " \
                              "glyphicon-refresh\"></span> <b>Refresh</b></button> <br><br>"
    return hello(clip_items_html=clip_items_html, form_action="action=\"\"", get_near=False,
                 share_button=share_button, start_or_add_note="",
                 viewing_clipboard_text=viewing_clipboard_text,
                 clipboard_code_header="<span class=\"special-slash-font\">s<small>/</small></span>" + str(gen_code),
                 return_symbol=return_symbol)


def share_local_page(gen_code):
    create_geolocation_file(gen_code)
    base_url = url_for("hello")
    print(base_url)
    final_url = base_url + str(gen_code)
    return redirect(final_url)


def share_local_button_html(gen_code, shared_already=False):
    if not shared_already:
        url = "/" + gen_code + "-SHARE_LOCAL"
        button_msg = "Share this Clipboard Locally"
        button_html = "<button onclick=\"location.href=\'" + url + "\'\" type=\"button\">" + button_msg + "</button>"
        return button_html
    else:
        return "<h5>This clipboard has been locally shared, and can be viewed on any nearby " \
               "devices by visiting http://myclip.is</h5> "


def code_currently_active(code):
    # return whether or not code is already assigned to an active clipboard
    bucket_name = CLIPBOARD_TIMESTAMP_BUCKET_NAME
    key_name = code + '.txt'
    # try to download the timestamp and see if it's already expired or not
    try:
        s3.Bucket(bucket_name).download_file(key_name, key_name)
        expiration_timestamp = int(open(key_name).read())
        curr_time = int(time.time())
        os.remove(key_name)  # need to remove the file after downloading or it stays on local flask filesystem
        if curr_time < expiration_timestamp:
            # timestamp has not yet expired
            return True
        else:
            # timestamp has expired
            return False
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise
    return False


def file_exists_on_s3(bucket, key):
    try:
        s3.Object(bucket, key).load()
        print("file exists")
        return True
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            # file did not exist, meaning clipboard has not yet been shared locally
            print("file doesn't exist")
            return False


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() not in BANNED_EXTENSIONS


# @app.route('/user/<username>', )


if __name__ == "__main__":
    Bootstrap(app)
    if not PRODUCTION_BUILD:
        app.debug = False
    else:
        app.debug = True
    app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
    app.run(host='0.0.0.0', port=80)
