import boto3
from boto3 import client
import botocore
import socket
import string
import random
import time
import os
from threading import Timer

# bind_ip = '0.0.0.0'
bind_ip = ''
bind_port = 9999
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((bind_ip, bind_port))
server.listen(20)  # max backlog of connections
print('Listening on {}:{}'.format(bind_ip, bind_port))

CLIPBOARD_DURATION_MINUTES = 30
URL_CODE_FILL_RATIO = 0.30  # percent of url code space allowed to be occupied at once
CLIPBOARD_FILE_STORAGE_BUCKET_NAME = "myclip.is.contents.clipboards.uniclip"
CLIPBOARD_GEO_LOC_BUCKET_NAME = "myclip.is.geolocations.clipboards.uniclip"
CLIPBOARD_TIMESTAMP_BUCKET_NAME = "myclip.is.timestamps.clipboards.uniclip"

s3 = boto3.resource('s3')
s3c = boto3.client('s3')


active_clipboards = 0
primed_clipboard = None


def expire_1_clipboard():
    if active_clipboards > 1:
        active_clipboards -= 1


def code_generator(size=3, chars=string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


def code_already_being_used(code):
    # return whether or not code is already assigned to an active clipboard
    bucket_name = CLIPBOARD_TIMESTAMP_BUCKET_NAME
    key_name = code + '.txt'
    # try to download the timestamp and see if it's already expired or not
    try:
        s3.Bucket(bucket_name).download_file(key_name, key_name)
        expiration_timestamp = int(open(key_name).read())
        curr_time = int(time.time())
        if curr_time < expiration_timestamp:
            # timestamp has not yet expired
            os.remove(key_name)  # need to remove the file after downloading or it stays on local flask filesystem
            return True
        else:
            # timestamp has expired
            os.remove(key_name)  # need to remove the file after downloading or it stays on local flask filesystem
            return False
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise
    return False


def prep_s3_dir(code):
    # transition clipboard bucket state

    # empty the bucket if it was being used before
    bucket = CLIPBOARD_FILE_STORAGE_BUCKET_NAME
    prefix = code + "/contents/"
    objects_to_delete = s3.meta.client.list_objects(Bucket=bucket, Prefix=prefix)
    delete_keys = {'Objects': []}
    delete_keys['Objects'] = [{'Key': k} for k in [obj['Key'] for obj in objects_to_delete.get('Contents', [])]]
    if len(delete_keys['Objects']) > 0:
        try:
            s3.meta.client.delete_objects(Bucket=bucket, Delete=delete_keys)
        except:
            print(str(delete_keys))
            print("Error deleting directory despite finding key")

    # delete any geolocation entries associated with the bucket
    s3.Object(CLIPBOARD_GEO_LOC_BUCKET_NAME, code + '.txt').delete()


def handle_timestamp(code):
    # add entry to timestamp bucket for deleting clipboard URL
    timestamp = time.time()
    # end_timestamp = start_timestamp + 5 * 60   this adds 5 minutes to a unix timestamp
    end_timestamp = str(int(timestamp + CLIPBOARD_DURATION_MINUTES * 60))
    s3.Object(CLIPBOARD_TIMESTAMP_BUCKET_NAME, code + '.txt').put(Body=end_timestamp)
    # TODO: utilize these timestamps to recycle URL codes in clipboard_recycle.py


def get_next_code():
    # get next URL code, also make any changes needed in S3 to transition directory state
    global active_clipboards
    active_clipboards += 1
    #t = Timer(60.0 * float(CLIPBOARD_DURATION_MINUTES), expire_1_clipboard)
    #t.start()

    # use algorithm to decide clipboard url length
    clipboard_reduction = 0
    world_size_holder = 1
    while world_size_holder < (active_clipboards / URL_CODE_FILL_RATIO):
        # url covers 26x more options per character added if restricted to only lowercase letters
        world_size_holder = world_size_holder * 26
        clipboard_reduction += 1
    code_length = max(2, clipboard_reduction)

    code = code_generator(size=code_length)
    # generate new code if generated one is currently being used
    while code_already_being_used(code):
        code = code_generator(size=code_length)
    prep_s3_dir(code)
    return code


def handle_client_connection(client_socket):
    request = client_socket.recv(1024).decode()
    print('Received {}'.format(request), flush=True)
    if request == 'GIVE_ME_A_CODE':
        # we want to do clipboard processing ahead of time, so we prime a clipboard code/directory after responding
        # to each new request; unless this is the first request - then we gen a new code to be sent out first
        global primed_clipboard
        if primed_clipboard is None:
            new_code = get_next_code()
            send_message(client_socket, new_code)
            # call timestamp handler only after sending a message to prevent early expiration
            handle_timestamp(new_code)
            primed_clipboard = get_next_code()
        else:
            send_message(client_socket, primed_clipboard)
            # call timestamp handler only after sending a message to prevent early expiration
            handle_timestamp(primed_clipboard)
            primed_clipboard = get_next_code()


def send_message(client_socket, message):
    client_socket.send(message.encode())
    client_socket.close()


while True:
    client_sock, address = server.accept()
    print('Accepted connection from {}:{}'.format(address[0], address[1]), flush=True)
    handle_client_connection(client_sock)
    # TODO: add way for recycling microservice to decrease active_clipboards (could be in batches, not 1 at a time)
